const rx = require('rxjs');
const Operators = require("rxjs/operators");
const cassandra = require('cassandra-driver');
const PlainTextAuthProvider = cassandra.auth.PlainTextAuthProvider;
const query = 'INSERT into smartirrigation.inputs (id, device_id, humidity, geo_location, water_level) values (now(), ?, ?, ?, ?)';

function CassandraManager(subject) {
    this._authProvider = new PlainTextAuthProvider('cassandra', 'cassandra');
    this._contactPoints = ['localhost:9042'];
    this._client = new cassandra.Client({contactPoints: this._contactPoints, localDataCenter: 'datacenter1', authProvider: this._authProvider, keyspace:'smartirrigation'});
    this._client.connect();
    this._subject = subject;
    this._subject.pipe(
        Operators.filter(x => x.indexOf("moisture=") >= 0))
        .subscribe(
            logLine => {
                let reading = logLine.split(",");
                let moisture = reading[1].split("=");
                let deviceId = reading[0].split("=");
                let humidity = parseInt(moisture[1].replace("\r", ""), 10);
                console.log('Sending data to Cassandra=' + logLine);
                new rx.Observable(this._client.execute(query, [deviceId[1], humidity, "-32.1919181,-4.3267088",  0.0], { prepare : true })).subscribe.bind(
                    y => console.log(y),
                    e => console.log('Error: ' + e.message));
            },
            e => console.log('Error: ' + e.message));
}

module.exports = CassandraManager;