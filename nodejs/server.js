const rx = require('rxjs');
const CassandraManager = require('./cassandra_manager');
const serialPort = require('serialport');
const portName = '/dev/cu.usbmodem14201';
const sp = new serialPort(portName, {
    baudRate: 9600,
    dataBits: 8,
    parity: 'none',
    stopBits: 1,
    flowControl: false
});

let subject = new rx.Subject();
let cassandraManager = new CassandraManager(subject);
let Readline = serialPort.parsers.Readline;
let parser = new Readline();
sp.pipe(parser);

parser.on('data', function(input) {
    subject.next(input);
});