int soilPin = A0;//Declare a variable for the soil moisture sensor 
int soilPower = 7;//Variable for Soil moisture Power
String macAdress = "32:fd:a9:16:F7:e1";
/*water pump and relay */
int relay_pin = 13;

void setup() 
{
  Serial.begin(9600);   // open serial over USB

  pinMode(soilPower, OUTPUT);//Set D7 as an OUTPUT
  digitalWrite(soilPower, LOW);//Set to LOW so no power is flowing through the sensor
  pinMode (relay_pin, OUTPUT);//Set D8 as an OUTPUT

  delay (2000);
}

void loop() 
{
    //get soil moisture value
    int moisture = readSoil();
    
    // format output for node.js receiver
    Serial.print("deviceid=");
    Serial.print(macAdress);
    Serial.print(",moisture=");
    Serial.println(moisture);    
    
    if(moisture < 99) {
      Serial.println("The soil is too dry");
      digitalWrite (relay_pin , LOW);
    }
    else if (moisture > 100 && moisture < 500) {
      Serial.println("The soil is good");
      digitalWrite (relay_pin , HIGH);
    }
    else if (moisture > 501) {
      Serial.println("The soil is too moist");
      digitalWrite (relay_pin , HIGH);
    }
    delay(1000);
}

int readSoil()
{
    int val = 0;
    digitalWrite(soilPower, HIGH);//turn D7 "On"
    delay(10);//wait 10 milliseconds 
    val = analogRead(soilPin);//Read the SIG value form sensor 
    digitalWrite(soilPower, LOW);//turn D7 "Off"
    return val;//send current moisture value
}
